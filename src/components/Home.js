import React from "react";
import { inject, observer } from "mobx-react";

@inject("user")
@observer
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.props.history.push("/edit");
  }

  render() {
    const { user } = this.props;
    return (
      <div>
        <strong>USER INFORMATION</strong>
        &nbsp;
        <button onClick={this.handleClick}>編集</button>
        {user.me.avatar.thumb && <div><img src={user.me.avatar.thumb} alt="自画像" style={{margin:"10px"}} /></div>}
        <p>ユーザーID：{user.me.name}</p>
        <p>本名：{user.me.realWorldName}</p>
        <p>SNS表示名：{user.me.displayName}</p>
        <p>メールアドレス：{user.me.email}</p>
        <p>電話番号：{user.me.tel}</p>
        <p>生年月日：{user.me.birthday}</p>
        <p>郵便番号：{user.me.address.zipCode}</p>
        <p>住所：{user.me.address.prefecture}{user.me.address.city}{user.me.address.address}</p>
        <p>PUSH通知設定：{(user.me.allowPushNotification && <span>受信する</span>) || <span>受信しない</span>}</p>
        <p>主に利用する店舗：{user.me.mainShop.name}</p>
        <p>メインの役割：{user.me.mainRole.name}</p>
        <p>自己紹介：{user.me.bio}</p>
        <div>アビリティ：<ul>{user.me.abilities.map(ability => (
          <li key={ability.id}>{ability.name}</li>
        ))}</ul></div>
      </div>
    )
  }
}
