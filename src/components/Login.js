import React from "react";
import { inject, observer } from "mobx-react";

@inject("user")
@observer
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChangeEmail(event) {
    const { user } = this.props;
    user.loginUser.email = event.target && event.target.value;
  }

  handleChangePassword(event) {
    const { user } = this.props;
    user.loginUser.password = event.target && event.target.value;
  }

  handleClick(event) {
    event.preventDefault();
    const { user } = this.props;
    if (user.loginUser.email.length > 0 && user.loginUser.password.length > 0) {
      user.login();
    }
  }

  componentDidUpdate() {
    if (this.props.user.isSignedIn) {
      this.props.history.push("/home");
    }
  }

  render() {
    const { user } = this.props;
    return (
      <div>
        <div><input type="email" onChange={this.handleChangeEmail} value={user.loginUser.email} /></div>
        <div><input type="password" onChange={this.handleChangePassword} value={user.loginUser.password} /></div>
        <div><button onClick={this.handleClick}>ログイン</button></div>
        {user.isSignedIn}
        {user.loginError.hasError && <strong style={{color:"red"}}>{user.loginError.errorMessage}</strong>}
      </div>
    )
  }
}
