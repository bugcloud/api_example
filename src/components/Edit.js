import React from "react";
import { inject, observer } from "mobx-react";

@inject("user")
@observer
export default class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: props.user.me.displayName,
      file: [],
      previewSrc: props.user.me.avatar.thumb,
      tel: props.user.me.tel,
      birthday: props.user.me.birthday,
      zipCode: props.user.me.address.zipCode,
      prefecture: props.user.me.address.prefecture,
      city: props.user.me.address.city,
      address: props.user.me.address.address,
      bio: props.user.me.bio,
      allowPushNotificationItems: ["受信する", "受信しない"],
      allowPushNotificationValue: "",
      mainShopId: 0,
      mainRoleId: 0,
      selectedAbilityIds: []
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeAbility = this.handleChangeAbility.bind(this);
    this.handleChangePreview = this.handleChangePreview.bind(this);
    this.handleClickSelectAll = this.handleClickSelectAll.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    let allowPushNotificationDraft = false;
    (this.state.allowPushNotificationValue === "受信する")
      ? allowPushNotificationDraft = true
      : allowPushNotificationDraft = false;
    const userInfoDraft = {
      display_name: this.state.displayName,
      tel: this.state.tel,
      birthday: this.state.birthday,
      address: {
        zip_code: this.state.zipCode,
        prefecture: this.state.prefecture,
        city: this.state.city,
        address: this.state.address
      },
      allow_push_notification: allowPushNotificationDraft,
      bio: this.state.bio,
      main_shop_id: this.state.mainShopId,
      main_role_id: this.state.mainRoleId,
      ability_ids: this.state.selectedAbilityIds
    }
    this.props.user.updateUserInfo(userInfoDraft, this.state.file);
  }

  handleChange(event) {
    const keyName = event.target.name;
    this.setState({[keyName]: event.target.value});
  }

  handleChangePreview(event) {
    this.setState({file: event.target.files[0]});
    this.setState({previewSrc: URL.createObjectURL(event.target.files[0])});
  }

  handleChangeAbility(event) {
    this.state.selectedAbilityIds.indexOf(Number(event.target.value)) >= 0
      ? this.setState({
          selectedAbilityIds: this.state.selectedAbilityIds.filter(id => id !== Number(event.target.value))
        })
      : this.setState({
          selectedAbilityIds: this.state.selectedAbilityIds.concat(Number(event.target.value))
        });
  }

  handleClickSelectAll(event) {
    this.state.selectedAbilityIds.length !== this.props.user.abilityList.length
      ? this.setState({
          selectedAbilityIds: this.props.user.abilityList.map(ability => ability.id)
        })
      : this.setState({
          selectedAbilityIds: []
        });
  }

  componentDidMount() {
    this.props.user.getAbilityList();
    this.setState({
      selectedAbilityIds: this.props.user.me.abilities.map(ability => ability.id)
    });
    this.props.user.getShopDigests();
    this.props.user.getRoleList();
    this.setState({mainShopId: this.props.user.me.mainShop.id});
    this.setState({mainRoleId: this.props.user.me.mainRole.id});
    (this.props.user.me.allowPushNotification)
      ? this.setState({allowPushNotificationValue: "受信する"})
      : this.setState({allowPushNotificationValue: "受信しない"});
  }

  componentDidUpdate() {
    if (this.props.user.editStatus.isEdited) {
      this.props.history.push("/home");
    }
  }

  render() {
    const { user } = this.props;
    const shopOptions = user.shopDigests.map(shop => (
      <option key={shop.id} value={shop.id} >{shop.name}</option>
    ));
    const roleOptions = user.roleList.map(role => (
      <option key={role.id} value={role.id} >{role.name}</option>
    ));
    return (
      <div>
        <strong>USER INFORMATION EDIT</strong>
        <p>SNS表示名：<input name="displayName" type="text" value={this.state.displayName} onChange={this.handleChange} /></p>
        {this.state.previewSrc && <div>
          <p>画像選択：</p>
          <img src={this.state.previewSrc} alt="自画像" style={{marginRight:"10px"}} />
          <input name="preview" type="file" accept="image/jpeg" onChange={this.handleChangePreview} />
          </div>
        }
        <p>電話番号：<input name="tel" type="text" value={this.state.tel} onChange={this.handleChange} /></p>
        <p>生年月日：<input name="birthday" type="text" value={this.state.birthday} onChange={this.handleChange} /></p>
        <p>郵便番号：<input name="zipCode" type="text" value={this.state.zipCode} onChange={this.handleChange} /></p>
        <p>都道府県：<input name="prefecture" type="text" value={this.state.prefecture} onChange={this.handleChange} /></p>
        <p>市区町：<input name="city" type="text" value={this.state.city} onChange={this.handleChange} /></p>
        <p>以降の住所：<input name="address" type="text" value={this.state.address} onChange={this.handleChange} /></p>
        <p>PUSH通知設定：{this.state.allowPushNotificationItems.map(item => (
          <label key={item}><input name="allowPushNotificationValue" type="radio"
            value={item}
            checked={this.state.allowPushNotificationValue === item}
            onChange={this.handleChange}/>{item}
          </label>))}
        </p>
        <p>主に利用する店舗：<select name="mainShopId" key={this.state.mainShopId} value={this.state.mainShopId} onChange={this.handleChange}>{shopOptions}</select></p>
        <p>メインの役割：<select name="mainRoleId" key={this.state.mainRoleId} value={this.state.mainRoleId} onChange={this.handleChange}>{roleOptions}</select></p>
        <div>自己紹介：<br />
          <textarea name="bio" defaultValue={this.state.bio} rows="5" cols="50" onChange={this.handleChange} />
        </div>
        <div>アビリティ：<button onClick={this.handleClickSelectAll}>すべて選択</button><br />
          {user.abilityList.map(ability =>
            <div key={ability.id}><label>
              <input type="checkbox" name="ability"
                value={ability.id}
                checked={this.state.selectedAbilityIds.indexOf(ability.id) >= 0}
                onChange={this.handleChangeAbility}
              />{ability.name}
            </label></div>
          )}
        </div><br />
        <button onClick={this.handleClick}>登録</button>
        {user.editStatus.isEdited}
        {user.editStatus.hasError && <strong style={{color:"red", marginLeft:"10px"}}>{user.editStatus.errorMessage}</strong>}
      </div>
    )
  }
}
