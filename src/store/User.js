import { observable, computed, action } from "mobx";
import axios from "axios";
// Disable CORS on browser temporarily
// open /Applications/Vivaldi.app/ --args --disable-web-security --user-data-dir

const client = axios.create({
  baseURL: "https://stg.enicia.me/api/v1",
  headers: { "Content-Type": "application/json" }
});

export default class User {
  @observable
  loginUser = {
    email: "",
    password: ""
  }

  @observable
  me = {
    authorization: "",
    name: "",
    realWorldName: "",
    displayName: "",
    email: "",
    tel: "",
    birthday: "",
    address: {
      zipCode: "",
      prefecture: "",
      city: "",
      address: ""
    },
    avatar: {
      large: "",
      thumb: ""
    },
    allowPushNotification: false,
    mainShop: {
      id: 0,
      name: ""
    },
    mainRole: {
      id: 0,
      name: "",
      alias: ""
    },
    bio: "",
    abilities: []
  }

  @computed get isSignedIn() {
    return (this.me.authorization !== "");
  }

  @observable
  loginError = {
    hasError: false,
    errorMessage: ""
  }

  @observable
  editStatus = {
    isEdited: false,
    hasError: false,
    errorMessage: ""
  }

  @observable shopDigests = [];
  @observable roleList = [];
  @observable abilityList = [];

  @action
  async login() {
    const data = {
      email: this.loginUser.email,
      password: this.loginUser.password
    }
    try {
      const res = await client.post("/users/sign_in", data);
      this.me.authorization = res.headers.authorization;
      await this.getUserInfo();
      this.loginError.hasError = false;
    } catch (error) {
      if (error.response.data.message) {
        this.loginError.errorMessage = error.response.data.message;
      } else {
        this.loginError.errorMessage = error.message;
      }
      this.loginError.hasError = true;
    }
  }

  @action
  async getUserInfo() {
    try {
      const res = await client.get("/me", {
        headers: { "Authorization": this.me.authorization }
      });
      this.me.name = res.data.name;
      this.me.realWorldName = res.data.real_world_name;
      this.me.displayName = res.data.display_name;
      this.me.email = res.data.email;
      this.me.tel = res.data.tel;
      this.me.birthday = res.data.birthday;
      this.me.address = {
        zipCode: res.data.address.zip_code,
        prefecture: res.data.address.prefecture,
        city: res.data.address.city,
        address: res.data.address.address
      };
      this.me.avatar = {
        large: res.data.avatar.large,
        thumb: res.data.avatar.thumb
      };
      this.me.allowPushNotification = res.data.allow_push_notification;
      this.me.mainShop = {
        id: res.data.main_shop.id,
        name: res.data.main_shop.name
      };
      this.me.mainRole = {
        id: res.data.main_role.id,
        name: res.data.main_role.name,
        alias: res.data.main_role.alias
      };
      this.me.bio = res.data.bio;
      this.me.abilities = res.data.abilities;
    } catch (error) {
      throw error;
    }
  }

  @action
  async updateUserInfo(data, file) {
    try {
      if (file.length !== 0) {
        const readFile = new Promise(resolve => {
          const reader = new FileReader();
          reader.onloadend = () => {
            resolve(reader.result);
          }
          reader.readAsDataURL(file);
        });
        await readFile.then(fileData => {
          data["avatar"] = fileData;
        });
      }
      await client.patch("/me/profiles", data, {
        headers: { "Authorization": this.me.authorization }
      });
      await this.getUserInfo();
      this.editStatus.hasError = false;
      this.editStatus.isEdited = true;
    } catch (error) {
      if (error.response.data.message) {
        this.editStatus.errorMessage = error.response.data.message
      } else {
        this.editStatus.errorMessage = error.message;
      }
      this.editStatus.hasError = true;
    }
  }

  @action
  async getShopDigests() {
    try {
      const res = await client.get("/shops/digests");
      this.shopDigests = res.data;
    } catch (error) {
      this.editStatus.errorMessage = error.message;
      this.editStatus.hasError = true;
    }
  }

  @action
  async getRoleList() {
    try {
      const res = await client.get("/roles");
      this.roleList = res.data;
    } catch (error) {
      this.editStatus.errorMessage = error.message;
      this.editStatus.hasError = true;
    }
  }

  @action
  async getAbilityList() {
    try {
      const res = await client.get("/abilities");
      this.abilityList = res.data;
    } catch (error) {
      this.editStatus.errorMessage = error.message;
      this.editStatus.hasError = true;
    }
  }
}
