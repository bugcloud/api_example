import React from "react";
import { Provider } from "mobx-react";
import { Route, Switch } from "react-router-dom";
import User from "./store/User.js"
import Login from "./components/Login.js"
import Home from "./components/Home.js"
import Edit from "./components/Edit.js"

const user = new User();

export default class App extends React.Component {
  render() {
    return (
      <Provider user={user}>
        <div>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/home" component={Home} />
            <Route path="/edit" component={Edit} />
          </Switch>
        </div>
      </Provider>
    )
  }
}
